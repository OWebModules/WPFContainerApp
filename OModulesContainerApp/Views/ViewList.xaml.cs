﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace OModulesContainerApp.Views
{
    /// <summary>
    /// Interaktionslogik für ViewList.xaml
    /// </summary>
    public partial class ViewList : Window
    {
        CefSharp.Wpf.ChromiumWebBrowser browserViewList = new CefSharp.Wpf.ChromiumWebBrowser();
        public ViewList()
        {
            InitializeComponent();
            browserViewList.Address = $"{ Properties.Settings.Default.BaseUrl }/ModuleMgmt/ModuleStarter";
            containerViewList.Content = browserViewList;
        }

        private void containerViewList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F5)
            {
                browserViewList.GetBrowser().Reload();
            }
            
        }
    }
}
