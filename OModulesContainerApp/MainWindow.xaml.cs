﻿using OModulesContainerApp.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OModulesContainerApp
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        delegate void ParametrizedMethodInvoker5(object sender, CefSharp.FrameLoadEndEventArgs e);

        private string urlClassTree;
        CefSharp.Wpf.ChromiumWebBrowser browserClassList = new CefSharp.Wpf.ChromiumWebBrowser();
        CefSharp.Wpf.ChromiumWebBrowser browserObjectList = new CefSharp.Wpf.ChromiumWebBrowser();

        public MainWindow()
        {
            InitializeComponent();
            urlClassTree = $"{ Properties.Settings.Default.BaseUrl }/ClassTree/ClassTree";
            browserClassList.Address = urlClassTree;
            browserClassList.FrameLoadEnd += BrowserClassList_FrameLoadEnd;
            containerClassTree.Content = browserClassList;
            containerObjectList.Content = browserObjectList;
            
        }

        private void BrowserClassList_FrameLoadEnd(object sender, CefSharp.FrameLoadEndEventArgs e)
        {
            
            if (e.Frame.IsMain)
            {
                if (e.Frame.Url == urlClassTree)
                {
                    if (!Dispatcher.CheckAccess()) // CheckAccess returns true if you're on the dispatcher thread
                    {
                        Dispatcher.Invoke(new ParametrizedMethodInvoker5(BrowserClassList_FrameLoadEnd), sender, e);
                        return;
                    }
                    this.colObjectList.Width = new GridLength(1.30, GridUnitType.Star);
                    this.browserObjectList.Address = $"{ Properties.Settings.Default.BaseUrl }/OItemList/ObjectList";
                }
                else
                {
                    if (!Dispatcher.CheckAccess()) // CheckAccess returns true if you're on the dispatcher thread
                    {
                        Dispatcher.Invoke(new ParametrizedMethodInvoker5(BrowserClassList_FrameLoadEnd), sender, e);
                        return;
                    }
                    this.colObjectList.Width = new GridLength(0, GridUnitType.Pixel);
                }
                
            }
        }

        

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var viewViewList = new ViewList();
            viewViewList.Show();
        }

        private void containerClassTree_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F5)
            {
                browserClassList.GetBrowser().Reload();
            }
        }

        private void containerObjectList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F5)
            {
                browserObjectList.GetBrowser().Reload();
            }
        }
    }
}
